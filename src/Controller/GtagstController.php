<?php

namespace App\Controller;

use App\Data\SearchData;
use App\Form\GtaGstType;
use App\Entity\Tgritarpst;
use App\SpecClass\Reponse;
use App\Form\SearchFormArt;
use App\Repository\TartfouRepository;
use App\Repository\TgritarettRepository;
use App\Repository\TgritarpstRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class GtagstController extends AbstractController
{
    /**
     * @Route("/gtagst/{idGta}", name="gtagst_aff")
     */
    public function gtagstAff($idGta, SessionInterface $session, Request $request, TgritarpstRepository $tgritarpstRepository, TgritarettRepository $tgritarettRepository, TartfouRepository $tartfouRepository)
    {

        if ($idGta === '0') {

            $idGta = $request->getSession()->get('vsIdGta', '0');
        }
        if ($idGta === '0') {
            $this->addFlash("warning", "Veuillez sélectionner une grille tarifaire");
            return $this->redirectToRoute("org_gst_bis");
            //   throw new HttpException(400, "La grille tarifaire n'est pas sélectionnée");
        }
        $request->getSession()->set('vsIdGta', $idGta);

        $tgritarett = $tgritarettRepository->findOneBy(['id' => $idGta]);
        //   $tgritarpst = $tgritarpstRepository->getTgritarpst($idGta, 5000);
        // $tgritarpst = $tgritarpstRepository->findBy(['clrGta' => $idGta], ['clrArt' => 'Asc']);

        $data = new SearchData;
        $data2 = new SearchData;
        $form = $this->createForm(SearchFormArt::class, $data, ['allow_extra_fields' => '00']);
        $form->handleRequest($request);
        $maxResult = 5000;
        $tgritarpst = $tgritarpstRepository->findSearch($data, $idGta, $maxResult);
        $tartfou = $tartfouRepository->findSearchPrxAch($data2, $idGta);
        $reponse = new Reponse;
        include "shared/_flashFiltreGta.txt";


        return $this->render('/gritargst/gtaGst.html.twig', [
            'tgritarett' => $tgritarett,
            'tgritarpst' => $tgritarpst,
            'tartfou' => $tartfou,
            'form' => $form->createView(),
            'allow_extra_fields' => '00'
        ]);
    }
    // /**
    //  * @Route("/gtagst2/{idGta}", name="gtagst2_aff")
    //  */
    // public function gtagst2Aff($idGta, SessionInterface $session, Request $request, TgritarpstRepository $tgritarpstRepository, TgritarettRepository $tgritarettRepository)
    // {
    // ;
    //     if ($idGta === '0') {
    
    //         $idGta = $request->getSession()->get('vsIdGta', '0');
    //     }
    //     if ($idGta === '0') {
    //         throw new HttpException(400, "La grille tarifaire n'est pas sélectionnée");
    //     }
    //     $request->getSession()->set('vsIdGta', $idGta);

    //     // dump($request->getSession()->get('vsIdGta'));

    //     $tgritarett = $tgritarettRepository->findOneBy(['id' => $idGta]);
    //     $tgritarpst = $tgritarpstRepository->findBy(['clrGta' => $idGta], ['clrArt' => 'Asc']);

    //     $data = new SearchData;

    //     $data2 = new Tgritarpst;

    //     $form = $this->createForm(SearchFormArt::class, $data);
    //     $form->handleRequest($request);
    //     $tgritarpst = $tgritarpstRepository->findSearch($data, $idGta,);
    //     $form2 = $this->createForm(GtaGstType::class, $tgritarpst);
    //     $form2->handleRequest($request);
    //     //    dd($form2);
    //     return $this->render('/gritargst/gtaGstBis.html.twig', [
    //         'tgritarett' => $tgritarett,
    //         'tgritarpst' => $tgritarpst,
    //         // 'form' => $form->createView(),
    //         // 'allow_extra_fields' => '01',
    //         'form2' => $form2->createView(),
    //     ]);
    // }
    /**
     * @Route("/gtagst/tot/enr", name="gtagst_enr")
     */
    public function gtagstEnr(SessionInterface $session, Request $request, TgritarpstRepository $tgritarpstRepository, TgritarettRepository $tgritarettRepository)
    {
        dd($request);
    }
    /**
     * @Route("/cml/gritarpst/supp/{id}", name="gtapst_supp")
     */
    public function gritarpstSupp($id, TgritarpstRepository $tgritarpstRepository, Request $request, EntityManagerInterface $em): Response
    {
        $tgritarpst = $tgritarpstRepository->findOneBy(['id' => $id]);

        $em->remove($tgritarpst);
        $em->flush();
        $this->addFlash("success", "Enregistrement supprimé");

        $idGta = $request->getSession()->get('vsIdGta', '0');

        return $this->redirectToRoute("gtagst_aff", ['idGta' => $idGta]);
    }
}
